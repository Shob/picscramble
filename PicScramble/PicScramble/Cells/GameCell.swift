//
//  GameCell.swift
//  PicScramble
//
//  Created by Shobhan Vijay M on 08/08/16.
//  Copyright © 2016 Shobhan Vijay M. All rights reserved.
//

import UIKit

class GameCell: UICollectionViewCell {

    @IBOutlet weak var cardView : UIView!
    @IBOutlet weak var frontView : UIView!
    @IBOutlet weak var backView : UIView!
    
    @IBOutlet weak var imageContainer: UIImageView!
    @IBOutlet weak var imageLoader: UIActivityIndicatorView!
    
    var imageURL: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let shadowPath = UIBezierPath(rect: cardView.bounds)
        cardView.layer.masksToBounds = false
        cardView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        cardView.layer.shadowOpacity = 0.3
        cardView.layer.shadowPath = shadowPath.CGPath
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageLoader.startAnimating()
    }
    
    func setImageURL(urlString: String, completion: (Void -> Void)) {
        imageContainer.sd_setImageWithURL(NSURL(string: urlString)) { (image, error, cache, url) in
            self.imageURL = urlString
            self.imageLoader.stopAnimating()
            completion()
        }
    }

    func flipView() {
        if imageURL.characters.count > 0 {
            UIView.transitionWithView(self, duration: 0.5, options: .TransitionFlipFromLeft, animations: {
                if self.frontView.hidden {
                    self.frontView.hidden = false
                } else {
                    self.frontView.hidden = true
                }
            }, completion: nil)
        }
    }
}
