//
//  GameViewController.swift
//  PicScramble
//
//  Created by Shobhan Vijay M on 08/08/16.
//  Copyright © 2016 Shobhan Vijay M. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import JLToast

class GameViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var gameTimerLabel: UILabel!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var gameCollectionView: UICollectionView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var questionImage: UIImageView!
    @IBOutlet weak var bottomLabel: UILabel!
    
    var imageLinks = [String]()
    var imageLoaded = [String : Bool]() {
        didSet {
            if imageLoaded.count > 0 {
                for currObj in imageLoaded {
                    if !currObj.1 {
                        return
                    }
                }
                screenReady()
            }
        }
    }
    
    var secondsToStart = 15
    var secondsCompleted : Float = 0
    var timer: NSTimer!
    
    var questionImageLink: String = ""
    
    var wrongImageCount = 0
    var wrongText = ["Not that!!!", "Wrong again!", "Try harder!", "Aw! Nope!!!"]
    var rightText = ["Gotcha", "Cool!!!", "Tats how you play!!!", "Keep Going..."]
    
    var totalAttempts = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        gameCollectionView.registerNib(UINib(nibName: "GameCell", bundle: nil), forCellWithReuseIdentifier: "GameCell")
        
        let shadowPath = UIBezierPath(rect: bottomView.bounds)
        bottomView.layer.masksToBounds = false
        bottomView.layer.shadowColor = UIColor.blackColor().CGColor
        bottomView.layer.shadowOpacity = 0.3
        bottomView.layer.shadowPath = shadowPath.CGPath
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        secondsToStart = 15
        secondsCompleted = 0
        totalAttempts = 0
        questionImageLink = ""
        questionImage.image = nil
        wrongImageCount = 0
        imageLinks = []
        imageLoaded = [:]
        bottomLabel.text = ""
        gameTimerLabel.text = "00:00"
        self.loadingView.alpha = 1
        self.loadingView.hidden = false
        self.gameCollectionView.reloadData()
        
        let request = Alamofire.request(.GET, "https://api.flickr.com/services/feeds/photos_public.gne", parameters: ["format": "json", "nojsoncallback":"1"])
        request.response { (request, response, data, error) in
            if error != nil {
              JLToast.makeText(error!.localizedDescription, duration: 3).show()
            }
            
            var dataString = String(data: data!, encoding: NSUTF8StringEncoding)
            
            dataString = dataString?.stringByReplacingOccurrencesOfString("\'", withString: "\"") // Bug in Flickr!
            
            let jsonData = dataString?.dataUsingEncoding(NSUTF8StringEncoding)
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(jsonData!, options: .AllowFragments)
                let items = json["items"] as! [[NSObject: AnyObject]]
                
                for item in items {
                    let imageLink = item["media"]!["m"] as! String
                    self.imageLinks.append(imageLink)
                    self.imageLoaded[imageLink] = false
                    if self.imageLinks.count == 9 {
                        break
                    }
                }
                
                self.gameCollectionView.reloadData()
                
                self.bottomLabel.text = "Images Loading... Be Ready!!!"

                UIView.animateWithDuration(0.5, animations: {
                    self.loadingView.alpha = 0
                    }, completion: { (completed) in
                        self.loadingView.hidden = true
                })
                
            } catch let error as NSError{
                JLToast.makeText(error.localizedDescription, duration: 3).show()
            }
        }
    }
    
    func screenReady() {
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(gameAboutToStart), userInfo: nil, repeats: true)
    }
    
    func gameAboutToStart() {
        self.bottomLabel.text = "Game starts in \(secondsToStart) seconds"
        secondsToStart -= 1
        if secondsToStart == 0 {
            timer.invalidate()
            
            for i in 0..<9 {
                let cell = gameCollectionView.cellForItemAtIndexPath(NSIndexPath(forRow: i, inSection: 0)) as! GameCell
                cell.flipView()
            }
            
            findImage()
            
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(gameTimerStarted), userInfo: nil, repeats: true)
        }
    }
    
    func findImage() {
        wrongImageCount = 0
        
        let index = arc4random_uniform(UInt32(imageLinks.count))
        questionImageLink = imageLinks[Int(index)]
        
        self.bottomLabel.text = "Find this Image."
        
        UIView.transitionWithView(questionImage, duration: 0.5, options: .TransitionCrossDissolve, animations: { 
            self.questionImage.sd_setImageWithURL(NSURL(string: self.questionImageLink))
            }) { (completed) in
                
        }
    }
    
    func gameTimerStarted() {
        dispatch_async(dispatch_get_main_queue()) { 
            self.gameTimerLabel.text = self.getTimerString()
        }
        
        secondsCompleted += 1
    }
    
    func getTimerString() -> String {
        let minutes = floor(secondsCompleted/60)
        let seconds = round(secondsCompleted - minutes * 60)
        
        let minuteString = minutes >= 10 ? "\(Int(minutes))" : "0\(Int(minutes))"
        let secondsString = seconds >= 10 ? "\(Int(seconds))" : "0\(Int(seconds))"
        
        return minuteString + ":" + secondsString
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        var maxWidth = (collectionView.bounds.size.width) / 3
        if maxWidth > 125 {
            maxWidth = 125
        }
        
        return CGSizeMake(maxWidth, maxWidth)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        var maxWidth = (collectionView.bounds.size.width) / 3
        if maxWidth > 125 {
            maxWidth = 125
        }
        
        let inset = (collectionView.bounds.size.width - (maxWidth * 3)) / 2
        
        return UIEdgeInsetsMake(20, inset, 0, inset)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("GameCell", forIndexPath: indexPath) as! GameCell
        if imageLinks.count > indexPath.row {
            let currImageLink = imageLinks[indexPath.row]
            cell.setImageURL(currImageLink, completion: { (completed) in
                self.imageLoaded[currImageLink] = true
            })
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if questionImageLink.characters.count > 0 {
            
            totalAttempts += 1

            let cell = collectionView.cellForItemAtIndexPath(indexPath) as! GameCell
            if cell.imageURL == questionImageLink {
                let index = Int(arc4random_uniform(UInt32(rightText.count)))
                bottomLabel.text = rightText[index]
                imageLinks.removeAtIndex(imageLinks.indexOf(questionImageLink)!)
                cell.flipView()
                
                if imageLinks.count > 0 {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
                        self.findImage()
                    })
                } else {
                    timer.invalidate()
                    self.performSegueWithIdentifier("toScore", sender: nil)
                }
                
            } else {
                wrongImageCount += 1
                if wrongImageCount == 1 {
                    bottomLabel.text = "Not that!!!"
                } else {
                    let index = Int(arc4random_uniform(UInt32(wrongText.count)))
                    bottomLabel.text = wrongText[index]
                }
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toScore" {
            let scoreVC = segue.destinationViewController as! ScoreViewController
            scoreVC.timerText = getTimerString()
            scoreVC.totalAttempts = totalAttempts
        }
    }
}
