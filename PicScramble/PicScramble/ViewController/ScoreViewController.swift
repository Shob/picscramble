//
//  ScoreViewController.swift
//  PicScramble
//
//  Created by Shobhan Vijay M on 09/08/16.
//  Copyright © 2016 Shobhan Vijay M. All rights reserved.
//

import UIKit

class ScoreViewController: UIViewController {

    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var successLabel: UILabel!
    @IBOutlet weak var attemptsLabel: UILabel!
    @IBOutlet weak var playAgainButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    
    var timerText: String!
    var totalAttempts = 0
    
    var completedString = ["Awesome", "Thats really cool...", "Wanna try again?", "Cool", "We should play again!"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playAgainButton.layer.cornerRadius = playAgainButton.bounds.size.height/2
        playAgainButton.layer.borderColor = UIColor.whiteColor().CGColor
        playAgainButton.layer.borderWidth = 1
        
        homeButton.layer.cornerRadius = playAgainButton.bounds.size.height/2
        homeButton.layer.borderColor = UIColor.whiteColor().CGColor
        homeButton.layer.borderWidth = 1
        
        timerLabel.text = timerText
        
        let index = Int(arc4random_uniform(UInt32(completedString.count)))
        successLabel.text = completedString[index]
        
        attemptsLabel.text = "Total Attempts: \(totalAttempts)"
    }

    @IBAction func homeButtonPressed(sender: AnyObject) {
        var vc = self.presentingViewController
        while (vc!.presentingViewController != nil) {
            vc = vc?.presentingViewController
        }
        vc?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func playAgainPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
