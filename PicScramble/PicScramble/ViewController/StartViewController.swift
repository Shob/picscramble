//
//  StartViewController.swift
//  PicScramble
//
//  Created by Shobhan Vijay M on 08/08/16.
//  Copyright © 2016 Shobhan Vijay M. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    @IBOutlet weak var mainLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var playButton: UIButton!
    
    var isFirstTime = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playButton.layer.borderColor = UIColor.whiteColor().CGColor
        playButton.layer.borderWidth = 1
        playButton.layer.cornerRadius = playButton.bounds.size.height / 2
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFirstTime {
            isFirstTime = false
            UIView.animateWithDuration(0.5, delay: 0.5, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: .CurveEaseInOut, animations: {
                self.mainLabelBottomConstraint.constant = 200
                self.view.layoutIfNeeded()
                }, completion: { (completed) in
                    
            })
        }
    }
    
    @IBAction func playButtonPressed(sender: AnyObject) {
        self.performSegueWithIdentifier("startGame", sender: nil)
    }

}

